/**
 * Array Rotation
 * --------------
 * Create a method that can rotate an array to the right by j elements.
 *
 * Assume reasonable constraints, e.g. integers only, array isn't empty, etc.
 */

/**
 * Rotate array `input` to the right by `shift` elements.
 *
 * O(n) time complexity (constant time)
 *
 * @param input
 * @param shift
 * @returns
 */
export function rotateArrRight<Type>(input: Type[], shift: number): Type[] {
  for (let j = 0; j < shift; j++) {
    let curLastElem = input[input.length - 1];
    for (let k = 0; k < input.length; k++) {
      let kElem = input[k];
      input[k] = curLastElem;
      curLastElem = kElem;
    }
  }

  return input;
}
